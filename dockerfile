FROM openjdk:17
COPY ./target/spring_crud_app-0.0.1-SNAPSHOT.jar back.jar
CMD [ "java", "-jar", "back.jar" ]